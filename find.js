function find(elements, cb) {
  try {
    for (let i = 0; i < elements.length; i++) {
      if (cb(elements[i])) {
        return elements[i];
      }
    }
  } catch (error) {
    console.error("Error occurred:", error.message);
  }
}

module.exports=find;