const each=require('./forEach.js');

function map(elements, cb) {
  try {
    const result = [];
    each(elements, element => {
      result.push(cb(elements[i], element));
    });
    return result;
  } catch (error) {
    console.error("Error occurred:", error.message);
  }
}

module.exports = map;
