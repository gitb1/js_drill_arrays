const each=require('./forEach.js');


function filter(elements, cb) {
  try{
    const result = [];
     each(elements, element => {
        if (cb(element)) {
            result.push(element);
         }
      });
     return result;
    }catch(error){
         console.error("Error occurred:", error.message);
    }
}

module.exports=filter;