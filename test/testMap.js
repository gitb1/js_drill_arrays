
const map = require ('../map.js');

const items = require ('../datasetForArrayHOF.js');

try{
 const mapped = map(items, element => element * 2);
 console.log(mapped);

}catch(error){
    console.error("Error occurred while testing 'map' function:", error.message);
}