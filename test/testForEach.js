const each = require ('../forEach.js');

const items = require ('../datasetForArrayHOF.js');

try {
    
    each(items, (item, index) => {
        console.log(`Item at index ${index}: ${item}`);
    });
} catch (error) {
 
    console.error("Error occurred while testing 'each' function:", error.message);
}