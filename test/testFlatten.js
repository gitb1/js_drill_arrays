const flatten = require ('../flatten.js');

const nestedArray = require ('../datasetForFlattening.js');

try{
   const flattened = flatten(nestedArray);
   console.log(flattened);
}catch(error){
    console.error("Error occurred while testing 'flatten' function:", error.message);
}